# Sample Configs
This is a collection of default config files for the various services. Start with these when configuring a deployment. Place them somewhere else, like the config folder or production_config folder or something. These should probably not be used as is.
Make sure to include the location of your config_folder in the .gitignore file to avoid pushing your real config back into the git repo.
