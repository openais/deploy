version: '3.7'
#-----------------
# This launches several containers that are used to process AIS data, from files,
# into a PostGIS+TimeScaleDB database. The traffic flow is:
# 
# AIS Hub Streamer > ais_decoder > rabbitmq > db_inserter > db
# 
# This file is intended to be launched with docker-compose. 
# Docker-compose can be used to spit out a swarm ready yaml file:
# https://stackoverflow.com/questions/44694640/docker-swarm-with-image-versions-externalized-to-env-file
#-----------------

networks:
  back_end:
    name: ais_backend
    # external: true
  front_end:
    name: ais_frontend

services:
  aishub_streamer:
    # Reads TCP port from AIShub
    hostname: aishub_streamer
    image: ${INGESTOR_IMAGE}:${INGESTOR_TAG} 
    networks:
      - back_end
    env_file:
      - .env  #This both the docker-compose env file (to set ports names, image tags etc) and the internal container config (handles passwords, ports to connect to etc.)
    command: python /usr/local/ais_i_mov/main.py -ll 'INFO'
    restart: unless-stopped 
    logging:
      driver: json-file
      options:
        max-size: 10m 

  ais_decoder:
    # Reads a folder full of daily AIS logs, decodes them and feeds them onto a rabbitmq topic 
    image: ${DECODER_IMAGE}:${DECODER_TAG} 
    networks:
      - back_end
    deploy:
      restart_policy:
        condition: on-failure
      replicas: 2 #This container is a bit of a CPU hog, so scaling it out is needed to keep up with the data stream
      resources:
        limits: 
          memory: 30M 
    env_file:
      - .env  #This both the docker-compose env file (to set ports names, image tags etc) and the internal container config (handles passwords, ports to connect to etc.)
    volumes:
        - /etc/localtime:/etc/localtime:ro #This is useful in Linux, doesn't work on Windows. 
      #- ${DATA_FILE_DIR}:${CONTAINER_FILE_DIR}
    command: python /usr/local/ais_decoder/main.py  -ll 'INFO'
    logging:
      driver: json-file
      options:
        max-size: 10m 


  rabbit:
  # Pub/Sub message broker that routes all the AIS messages
  # to the various processing containers
    hostname: ${RABBIT_HOST}
    image: ${RABBIT_IMAGE}:${RABBIT_TAG}
    ports:
      - ${RABBIT_MSG_PORT}:5672
      - ${RABBIT_MANAGE_PORT}:15672
    networks:
      - back_end
    environment: 
        RABBITMQ_DEFAULT_USER: ${RABBITMQ_DEFAULT_USER}
        RABBITMQ_DEFAULT_PASS: ${RABBITMQ_DEFAULT_PASS}
    ulimits:
      nproc: 65535
      nofile:
        soft: 20000
        hard: 40000  
    volumes:
      - rabbit_data:/var/lib/rabbitmq/
      - rabbit_etc:/etc/rabbitmq/
    restart: unless-stopped
    logging:
      driver: json-file
      options:
        max-size: 10m 

  db_inserter: 
  # Builds a queue that's connected to the file_source output. Packages the messages into a DB acceptable format and 
  # sends them to an async worker to do the actual insert. It's much faster to do bulk inserts than it is to try
  # insert every single message alone.  
    hostname: db_inserter
    image: ${INSERTER_IMAGE}:${INSERTER_TAG} 
    networks:
      - back_end
    # The .env file is both the docker-compose env file (to set ports names, image tags etc) 
    # and the internal container config (handles passwords, ports to connect to, etc.)
    env_file:
      - .env 
    environment:
      #   Handled by secrets:
       POSTGRES_USER_FILE: /run/secrets/POSTGRES_USER_FILE
       POSTGRES_PASSWORD_FILE: /run/secrets/POSTGRES_PASSWORD_FILE
    volumes:
       - /etc/localtime:/etc/localtime:ro    #This is useful in Linux, doesn't work on Windows. 
    command: python /usr/local/db-sink/main.py -ll 'INFO' 
    restart: unless-stopped
    logging:
      driver: json-file
      options:
        max-size: 10m
    secrets:
      # Currently there are a limited amount of secrets that can be used as stand-in's for enviroment variables
      # see the "Docker Secrets" section of https://hub.docker.com/_/postgres
      - POSTGRES_PASSWORD_FILE
      - POSTGRES_USER_FILE 
        
  ais_db:
  # The database that holds all the AIS data and maybe some other nice to have's like:
  #   - World Port Index
  #   - World EEZ shapefile (And other MarineRegions stuff)
  #   - AIS protocol helper info (number-to-descriptions etc)
  # The schema is also set up in the start up scripts.  
  # https://github.com/timescale/timescaledb-docker/pull/94
  # docker pull timescale/timescaledb-ha:pg12.5-ts2.0.0-p0
    # hostname: ${DB_HOSTNAME}
    # Dockerfile below has wget and shp2pgsql installed on it
    # build: ./build/db/.
    image: ${DB_IMAGE}:${DB_TAG}
    shm_size: 12g
    command: postgres -c shared_preload_libraries=timescaledb -c 'config_file=/etc/postgresql/postgresql.conf'
    ports:
      - ${DB_EXT_PORT}:5432
    environment:
      #   Handled by secrets:
       POSTGRES_USER_FILE: /run/secrets/POSTGRES_USER_FILE
       POSTGRES_PASSWORD_FILE: /run/secrets/POSTGRES_PASSWORD_FILE
       POSTGRES_DB_FILE: /run/secrets/POSTGRES_DB_FILE
      #  POSTGRES_UID: ${UID}
      #  POSTGRES_GID: ${GID}
      #  PGDATA: ${PG_DATA}/pgdata
    networks:
      - back_end
    volumes:
      - ./volumes/pgdata:${PG_DATA}
      - ./build/db/db_init:/docker-entrypoint-initdb.d/ 
      - ./build/db/db_init_data/:/tmp
      - ./config/postgresql.conf:/etc/postgresql/postgresql.conf
    restart: unless-stopped
    logging:
      driver: json-file
      options:
        max-size: 10m
    secrets:
      # Currently there are a limited amount of secrets that can be used as stand-in's for enviroment variables
      # see the "Docker Secrets" section of https://hub.docker.com/_/postgres
      - POSTGRES_PASSWORD_FILE
      - POSTGRES_USER_FILE
      - POSTGRES_DB_FILE

  featserv:
    # Pg Featureserv: Lightweight PostGIS API tool
    # https://access.crunchydata.com/documentation/pg_featureserv/1.1.1/introduction/
    hostname: featserv
    image: ${FEATSERV_IMAGE}:${PGFEATSERV_TAG}
    ports:
      - ${PGFEATSERV_PORT}:9000
    networks:
      - back_end 
      - front_end
    volumes:
      - ./config/${PGFEATSERV_CFG_FILE}:/config/pg_featureserv.toml 
    # Standard stuff
    restart: unless-stopped
    logging:
      driver: json-file
      options:
        max-size: 10m

  dashboard:
    # Uses plotly dash to do basic API calls to the database. Not part of any network as 
    # it must consume off the public API. 
    hostname: dashboard
    networks:
      - front_end
    image: ${DASHBOARD_IMAGE}:${DASHBOARD_TAG}
    ports:
      - ${DASHBOARD_PORT}:80
    # Standard stuff
    restart: unless-stopped
    logging:
      driver: json-file
      options:
        max-size: 10m

# ---------------------------------------------
# DOCKER EXTRAS: storing data, routing traffic, etc
# ---------------------------------------------
volumes:
  rabbit_data:
  rabbit_etc:
  db_data:  
  pg_admin_config:
    
secrets:
#NOTE: for convienience the DB_URL secret has duplicate info to 
# what's stored in the other PG secrets. So changing the password
# , for example, would require you to change the password file and 
# the DB_URL file
  POSTGRES_PASSWORD_FILE:
    file: ./secrets/postgres_pw.txt
  POSTGRES_USER_FILE:
    file: ./secrets/postgres_user.txt
  POSTGRES_DB_FILE:
    file: ./secrets/postgres_db.txt
  POSTGRES_URL_FILE:
    file: ./secrets/db_url.txt
