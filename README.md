---
# Hugo K/V pairs 
title: Data Pipeline Deployement
comments: false
weight: 40
---

<!-- # OpenAIS Deployment -->

This project holds the config and other files required to allow a deployment of the OpenAIS system onto a Linux system.

 ![Big'ol Block Diagram](/docs/BlockDiagram.png)

This deployment pulls prebuilt containers from the Gitlab project repository, configures them using an environment variable file and more specific config files. The docker-compose file allows the deployment of
  - an AIS preprocessing chain:
    - AIS-i-Mov: an AIS parser
    - AIS Decoder: converts NMEA encoded AIS into a decoded JSON object. 2x of these are deployed to handle scaling issues
    - DB Inserter: Inserts JSON messages into the database
  - Database: PostgreSQL+PostGIS+TimescaleDB database
  - FrontEnd:
    - PG-FeatureServ: OGC compliant API service
    - Dashboard: Python generated dummy dashboard. Not really the best but it's something...
  - Management:
    - PGAdmin: SQL and DB admin tool. 
    - Portainer: Container management tool.

## Required
The following is required for this deployment to work:
  - Server with Linux operating system with
  - Docker and
  - Docker Compose installed

## Getting started

To get the project running do the following:
  - Pull/clone this repo to your target machine
  - Copy and edit the ./sample_config/sample.env file to .env
  - Copy and edit the files in ./sample_config/\* to /config/\*
  - docker-compose pull (to get the latest version of the containers)
  - docker-compose up -d 

NOTE: Currently the DB that is deployed in the project doesn't have any AIS tables or functions. These need to be created as an additional step after deployement. 

